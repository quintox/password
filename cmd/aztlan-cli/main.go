package main

import (
	"database/sql"
	"errors"
	"fmt"
	"os"
	"sort"
	"strconv"
	"syscall"

	_ "github.com/mattn/go-sqlite3"
	crypto "gitlab.com/quintox/crypto"
	password "gitlab.com/quintox/password"
	terminal "golang.org/x/crypto/ssh/terminal"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
)

var (
	db *sql.DB

	database = kingpin.Flag("database", "Database file.").Short('d').
			Default("aztlan.sqlite3").Envar("AZTLAN_DATABASE").String()
	master = kingpin.Flag("password", "Database master passowrd.").Short('p').
		Envar("AZTLAN_PASSWORD").String()

	insertCmd = kingpin.Command("insert", "Insert a new entry.").Alias("i").Alias("new").Alias("n")
	iName     = insertCmd.Arg("name", "Entry name").Required().String()
	iPass     = insertCmd.Arg("pass", "Entry password").Required().String()
	iTags     = insertCmd.Arg("tag", "Entry tags").Strings()
	iData     = insertCmd.Flag("attr", "Entry data attributes").Short('a').StringMap()
	iEncData  = insertCmd.Flag("eattr", "Entry encrypted data attributes").Short('e').StringMap()

	updateCmd = kingpin.Command("update", "Modify an entry.").Alias("u").Alias("set")
	uPK       = updateCmd.Arg("pk", "Entry name or ID").Required().String()
	uName     = updateCmd.Flag("name", "New name").Short('n').String()
	uPass     = updateCmd.Flag("pass", "New password").Short('s').String()
	uTags     = updateCmd.Flag("tags", "Entry Tags").Short('t').Strings()
	uData     = updateCmd.Flag("attr", "Entry data attributes").Short('a').StringMap()
	uEncData  = updateCmd.Flag("eattr", "Entry encrypted data attributes").Short('e').StringMap()
	uClrTags  = updateCmd.Flag("clear-tags", "Clear all tags.").Bool()
	uClrEData = updateCmd.Flag("clear-eattrs", "Clear all ecnrypted data attributes.").Bool()
	uClrPData = updateCmd.Flag("clear-attrs", "Clear all none encrypted data attributes.").Bool()
	uClrData  = updateCmd.Flag("clear-all-attrs", "Clear all data attributes.").Bool()

	deleteCmd = kingpin.Command("delete", "Delete an entry.").Alias("d").Alias("rm")
	dPK       = deleteCmd.Arg("pk", "Entry name or ID").Required().String()

	readCmd = kingpin.Command("read", "Display an entry.").Alias("r").Alias("show").Alias("get").Alias("g")
	rPK     = readCmd.Arg("pk", "Entry name or ID").Required().String()

	searchCmd = kingpin.Command("search", "Search for entries by name, tag or none encrypted data value.").
			Alias("s").Alias("find").Alias("f")
	sStr = searchCmd.Arg("string", "Search string").Required().String()
)

func parseID(pk string) (int64, bool) {
	id, err := strconv.ParseInt(pk, 10, 64)
	if err != nil || id <= 0 {
		return -1, false
	}
	return id, true
}

func entryByPK(pk *string) (*password.Password, error) {
	if pk == nil {
		return nil, errors.New("Invalid name/ID")
	}

	if id, ok := parseID(*pk); ok {
		return password.ByID(db, id)
	}

	return password.ByName(db, *pk)
}

func insert() (*password.Password, error) {
	p := password.New(db, *iName, *iPass)

	if iTags != nil {
		p.Tags = *iTags
	}

	if iData != nil {
		for k, v := range *iData {
			p.Data[k] = password.DataItem{Encrypted: false, Val: v}
		}
	}

	if iEncData != nil {
		for k, v := range *iEncData {
			p.Data[k] = password.DataItem{Encrypted: true, Val: v}
		}
	}

	err := p.Save()

	return p, err
}

func update() (*password.Password, error) {
	p, err := entryByPK(uPK)
	if err != nil {
		return nil, err
	}

	if *uName != "" {
		p.Name = *uName
	}

	if *uPass != "" && p.Pass != *uPass {
		p.PrevPass = append([]string{p.Pass}, p.PrevPass...)
		p.Pass = *uPass
	}

	if *uClrTags {
		p.Tags = make([]string, 0)
	}

	p.Tags = append(p.Tags, *uTags...)

	*uClrData = *uClrData || (*uClrEData && *uClrPData)

	if *uClrData {
		p.Data = make(map[string]password.DataItem)
	} else if *uClrEData {
		for k, i := range p.Data {
			if i.Encrypted {
				delete(p.Data, k)
			}
		}
	} else if *uClrPData {
		for k, i := range p.Data {
			if !i.Encrypted {
				delete(p.Data, k)
			}
		}
	}

	for k, v := range *uData {
		p.Data[k] = password.DataItem{Encrypted: false, Val: v}
	}

	for k, v := range *uEncData {
		p.Data[k] = password.DataItem{Encrypted: true, Val: v}
	}

	err = p.Save()
	if err != nil {
		return nil, err
	}

	return password.ByID(db, p.ID)
}

func remove() (*password.Password, error) {
	p, err := entryByPK(uPK)
	if err != nil {
		return nil, err
	}

	return p, p.Delete()
}

func read() (*password.Password, error) {
	return entryByPK(rPK)
}

func search() ([]*password.Password, error) {
	return password.Search(db, *sStr)
}

func showPassword(p *password.Password) {
	if p.IsReadOnly() {
		fmt.Printf("%4d | %s", p.ID, p.Name)
		return
	}

	fmt.Printf("       ID: %d\n", p.ID)
	fmt.Printf("     Name: %s\n", p.Name)
	fmt.Printf("     Pass: %s\n", p.Pass)

	if len(p.PrevPass) > 0 {
		fmt.Printf(" PrevPass: %s\n", p.PrevPass[0])
		for i := 1; i < len(p.PrevPass); i++ {
			fmt.Printf("           %s\n", p.PrevPass[i])
		}
	}

	if len(p.Tags) > 0 {
		fmt.Printf("     Tags: %s\n", p.Tags[0])
		for i := 1; i < len(p.Tags); i++ {
			fmt.Printf("           %s\n", p.Tags[i])
		}
	}

	if len(p.Data) > 0 {
		encrypted := make([]string, 0, len(p.Data))
		plain := make([]string, 0, len(p.Data))

		for k, i := range p.Data {
			if i.Encrypted {
				encrypted = append(encrypted, k)
			} else {
				plain = append(plain, k)
			}
		}
		sort.Strings(encrypted)
		sort.Strings(plain)

		if len(encrypted) > 0 {
			fmt.Printf(" -- Encrypted Attributes --\n")
			for _, k := range encrypted {
				fmt.Printf(" %8s: %s\n", k, p.Data[k].Val)
			}
		}

		if len(plain) > 0 {
			fmt.Printf(" -- Plain Attributes --\n")
			for _, k := range plain {
				fmt.Printf(" %8s: %s\n", k, p.Data[k].Val)
			}
		}
	}
}

func handleError(err error) {
	// typ := reflect.TypeOf(err)
	// fmt.Printf("ERROR: %s/%s: %s\n", typ.PkgPath(), typ.Name(), err.Error())
	fmt.Printf("ERROR: %s\n", err.Error())
	os.Exit(1)
}

func initialize() error {
	passphrase := *master

	for len(passphrase) <= 0 {
		fmt.Print("Enter Master Password: ")
		bytePassword, err := terminal.ReadPassword(int(syscall.Stdin))
		if err != nil {
			return err
		}

		passphrase = string(bytePassword)
		fmt.Println()
	}

	err := crypto.SetKey(passphrase)
	if err != nil {
		return err
	}

	db, err = sql.Open("sqlite3", *database)
	if err != nil {
		return err
	}

	return password.InitDatabase(db)
}

func init() {
	kingpin.Version("0.0.1")

	kingpin.HelpFlag = kingpin.HelpFlag.Short('h')
}

func main() {
	var (
		p   *password.Password
		l   []*password.Password
		err error
	)

	cmd := kingpin.Parse()

	err = initialize()
	if err != nil {
		handleError(err)
	}

	switch cmd {
	case insertCmd.FullCommand():
		p, err = insert()
	case updateCmd.FullCommand():
		p, err = update()
	case deleteCmd.FullCommand():
		p, err = remove()
	case readCmd.FullCommand():
		p, err = read()
	case searchCmd.FullCommand():
		l, err = search()
	}

	if err != nil {
		handleError(err)
	}

	if p != nil {
		showPassword(p)
	}

	if l != nil {
		fmt.Println("  ID | Name")
		fmt.Println("-----|---------------------------------------------")
		for _, p := range l {
			showPassword(p)
		}
	}
}
