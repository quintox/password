package password

import "errors"

var (
	ErrInvalidData  = errors.New("invalid data")
	ErrInvalidEntry = errors.New("invalid entry")
	ErrReadOnly     = errors.New("read only entry")
)
