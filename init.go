package password

import "database/sql"

// InitDatabase .
func InitDatabase(db *sql.DB) error {
	if _, err := db.Exec(createEntryTable); err != nil {
		return err
	}

	if _, err := db.Exec(createPrevPassTable); err != nil {
		return err
	}

	if _, err := db.Exec(createDataTable); err != nil {
		return err
	}

	if _, err := db.Exec(createTagTable); err != nil {
		return err
	}

	if _, err := db.Exec(createEntryTagTable); err != nil {
		return err
	}

	return nil
}
