package password

import (
	"database/sql"
	"encoding/json"

	"gitlab.com/quintox/crypto"
)

// DataItem .
type DataItem struct {
	Encrypted bool   `json:"iencrypted"`
	Val       string `json:"val"`
}

// Password .
type Password struct {
	db *sql.DB

	ID       int64               `json:"id,omitempty"`
	Name     string              `json:"name,omitempty"`
	Pass     string              `json:"pass,omitempty"`
	PrevPass []string            `json:"prev_pass,omitempty"`
	Tags     []string            `json:"tags,omitempty"`
	Data     map[string]DataItem `json:"data,omitempty"`
}

func (p *Password) getTags() error {
	p.Tags = nil

	if p.db != nil && p.ID > 0 {
		tags, err := queryStringList(p.db, false, entryTags, p.ID)
		if err != nil {
			return err
		}

		p.Tags = tags
	}

	return nil
}

func (p *Password) getPrevPass() error {
	p.PrevPass = nil

	if p.db != nil && p.ID > 0 {
		prePass, err := queryStringList(p.db, true, entryPrevPass, p.ID)
		if err != nil {
			return err
		}

		p.PrevPass = prePass
	}

	return nil
}

func (p *Password) getData() error {
	p.Data = nil

	if p.db != nil && p.ID > 0 {
		rows, err := p.db.Query(entryData, p.ID)
		if err != nil {
			return err
		}
		defer rows.Close()

		data := make(map[string]DataItem)

		for rows.Next() {
			var (
				key       string
				encrypted bool
				val       []byte
			)

			err = rows.Scan(&key, &encrypted, &val)
			if err != nil {
				return err
			}

			if encrypted {
				val, err = crypto.Decrypt(val, nil)
				if err != nil {
					return err
				}
			}

			data[key] = DataItem{Encrypted: encrypted, Val: string(val)}
		}

		p.Data = data
	}

	return nil
}

func (p *Password) load() error {
	if err := p.getTags(); err != nil {
		return err
	}

	if err := p.getPrevPass(); err != nil {
		return err
	}

	if err := p.getData(); err != nil {
		return err
	}

	return nil
}

// IsReadOnly .
func (p *Password) IsReadOnly() bool {
	return p.db == nil
}

// Delete .
func (p *Password) Delete() error {
	if p.db == nil {
		return ErrReadOnly
	}

	if p.ID > 0 {
		tx, err := p.db.Begin()
		if err != nil {
			return err
		}

		_, err = tx.Exec(deleteEntryTags, p.ID)
		if err != nil {
			tx.Rollback()
			return err
		}

		_, err = tx.Exec(deleteEntryData, p.ID)
		if err != nil {
			tx.Rollback()
			return err
		}

		_, err = tx.Exec(deleteEntryPrevPass, p.ID)
		if err != nil {
			tx.Rollback()
			return err
		}

		_, err = tx.Exec(deleteEntry, p.ID)
		if err != nil {
			tx.Rollback()
			return err
		}

		err = tx.Commit()
		if err != nil {
			return err
		}
	}

	p.ID = 0

	return nil
}

// Save .
func (p *Password) Save() error {
	if p.db == nil {
		return ErrReadOnly
	}

	pass, err := crypto.Encrypt([]byte(p.Pass), nil)
	if err != nil {
		return err
	}

	tx, err := p.db.Begin()
	if err != nil {
		return err
	}

	if p.ID > 0 {
		_, err = tx.Exec(updateEntry, p.Name, pass, p.ID)
		if err != nil {
			tx.Rollback()
			return err
		}
	} else {
		rs, err := tx.Exec(insertEntry, p.Name, pass)
		if err != nil {
			tx.Rollback()
			return err
		}

		id, err := rs.LastInsertId()
		if err != nil {
			tx.Rollback()
			return err
		}

		p.ID = id
	}

	_, err = tx.Exec(deleteEntryTags, p.ID)
	if err != nil {
		tx.Rollback()
		return err
	}

	_, err = tx.Exec(deleteEntryData, p.ID)
	if err != nil {
		tx.Rollback()
		return err
	}

	_, err = tx.Exec(deleteEntryPrevPass, p.ID)
	if err != nil {
		tx.Rollback()
		return err
	}

	for _, tag := range p.Tags {
		_, err = tx.Exec(insertTag, tag)
		if err != nil {
			tx.Rollback()
			return err
		}

		_, err = tx.Exec(insertEntryTag, p.ID, tag)
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	for ord, pass := range p.PrevPass {
		epass, err := crypto.Encrypt([]byte(pass), nil)
		if err != nil {
			tx.Rollback()
			return err
		}

		_, err = tx.Exec(insertPrevPass, p.ID, epass, ord)
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	for key, item := range p.Data {
		var val []byte

		if item.Encrypted {
			val, err = crypto.Encrypt([]byte(item.Val), nil)
			if err != nil {
				tx.Rollback()
				return err
			}
		} else {
			val = []byte(item.Val)
		}

		_, err = tx.Exec(insertData, p.ID, key, item.Encrypted, val)
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	return tx.Commit()
}

// Update .
func (p *Password) Update(other *Password) (changed bool) {
	if other.Name != "" {
		p.Name = other.Name
		changed = true
	}

	if other.Pass != "" && other.Pass != p.Pass {
		p.PrevPass = append([]string{p.Pass}, p.PrevPass...)
		p.Pass = other.Pass
		changed = true
	}

	if other.Tags != nil {
		p.Tags = other.Tags
		changed = true
	}

	if other.Data != nil {
		p.Data = other.Data
		changed = true
	}

	return
}

// New .
func New(db *sql.DB, name, password string) *Password {
	return &Password{
		db: db,

		ID:       0,
		Name:     name,
		Pass:     password,
		PrevPass: make([]string, 0),
		Tags:     make([]string, 0),
		Data:     make(map[string]DataItem),
	}
}

func loadEntry(p *Password, row *sql.Row) error {
	var b []byte

	err := row.Scan(&p.ID, &p.Name, &b)
	if err != nil {
		return err
	}

	b, err = crypto.Decrypt(b, nil)
	if err != nil {
		return err
	}

	p.Pass = string(b)

	return p.load()
}

// ByID .
func ByID(db *sql.DB, id int64) (*Password, error) {
	if id <= 0 {
		return nil, sql.ErrNoRows
	}

	p := &Password{db: db}

	err := loadEntry(p, db.QueryRow(entryByID, id))
	if err != nil {
		return nil, err
	}

	return p, nil
}

// ByName .
func ByName(db *sql.DB, name string) (*Password, error) {
	if len(name) == 0 {
		return nil, sql.ErrNoRows
	}

	p := &Password{db: db}

	err := loadEntry(p, db.QueryRow(entryByName, name))
	if err != nil {
		return nil, err
	}

	return p, nil
}

// Search .
func Search(db *sql.DB, s string) ([]*Password, error) {
	result := make([]*Password, 0)

	if len(s) > 0 {
		rows, err := db.Query(searchEntries, s)
		if err != nil {
			return nil, err
		}
		defer rows.Close()

		for rows.Next() {
			p := new(Password)

			err = rows.Scan(&p.ID, &p.Name)
			if err != nil {
				return nil, err
			}

			result = append(result, p)
		}
	}

	return result, nil
}

// Count .
func Count(db *sql.DB) (count int, err error) {
	err = db.QueryRow(countEntries).Scan(&count)
	return
}

// Tags .
func Tags(db *sql.DB, s string) (tags []string, err error) {
	if len(s) > 0 {
		tags, err = queryStringList(db, false, searchTags, s)
	} else {
		tags, err = queryStringList(db, false, allTags)
	}
	return
}

// ParseJSON .
func ParseJSON(db *sql.DB, id int64, data []byte) (*Password, error) {
	if len(data) < 2 {
		return nil, ErrInvalidData
	}

	p := &Password{db: db}

	err := json.Unmarshal(data, p)
	if err != nil {
		return nil, ErrInvalidData
	}

	if db != nil {
		p.ID = 0
		if id > 0 {
			p.ID = id
		}

		if p.Name == "" || p.Pass == "" {
			return nil, ErrInvalidEntry
		}
	}

	return p, nil
}

// ParsePartialJSON .
func ParsePartialJSON(data []byte) (*Password, error) {
	return ParseJSON(nil, 0, data)
}
