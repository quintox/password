package password

import (
	"database/sql"
	"os"
	"testing"

	"gitlab.com/quintox/crypto"

	_ "github.com/mattn/go-sqlite3"
)

func TestPassword(t *testing.T) {
	const key = "top secret"

	err := crypto.SetKey(key)
	if err != nil {
		t.Fatal(err)
	}

	err = os.Remove("test-database.sqlite3")
	if err != nil && !os.IsNotExist(err) {
		t.Fatal(err)
	}

	db, err := sql.Open("sqlite3", "test-database.sqlite3")
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	err = InitDatabase(db)
	if err != nil {
		t.Fatal(err)
	}

	p := New(db, "Unix-DEV", "secret")

	err = p.Save()
	if err != nil {
		t.Fatal(err)
	}

	p, err = ByID(db, p.ID)
	if err != nil {
		t.Fatal(err)
	}

	if p.Name != "Unix-DEV" || p.Pass != "secret" {
		t.Fail()
	}

	p.Tags = []string{"TAG-Z", "TAG-B"}

	err = p.Save()
	if err != nil {
		t.Fatal(err)
	}

	p, err = ByID(db, p.ID)
	if err != nil {
		t.Fatal(err)
	}

	if len(p.Tags) != 2 || p.Tags[0] != "TAG-B" || p.Tags[1] != "TAG-Z" {
		t.Fail()
	}

	p.Data["cipher"] = DataItem{Encrypted: true, Val: "this should be encrypted"}
	p.Data["plain"] = DataItem{Encrypted: false, Val: "this should NOT be encrypted"}

	err = p.Save()
	if err != nil {
		t.Fatal(err)
	}

	p, err = ByName(db, "uniX-dev")
	if err != nil {
		t.Fatal(err)
	}

	if len(p.Data) != 2 || !p.Data["cipher"].Encrypted || p.Data["plain"].Encrypted {
		t.Fail()
	}

	if p.Data["cipher"].Val != "this should be encrypted" {
		t.Fail()
	}

	if p.Data["plain"].Val != "this should NOT be encrypted" {
		t.Fail()
	}

	l, err := Search(db, "uni")
	if err != nil {
		t.Fatal(err)
	}

	if len(l) != 1 {
		t.Fail()
	}

	l, err = Search(db, "una")
	if err != nil {
		t.Fatal(err)
	}

	if len(l) != 0 {
		t.Fail()
	}

	l, err = Search(db, "tag")
	if err != nil {
		t.Fatal(err)
	}

	if len(l) != 1 {
		t.Fail()
	}

	l, err = Search(db, "tag-a")
	if err != nil {
		t.Fatal(err)
	}

	if len(l) != 0 {
		t.Fail()
	}

	l, err = Search(db, "NOT be")
	if err != nil {
		t.Fatal(err)
	}

	if len(l) != 1 {
		t.Fail()
	}

	if l[0].Save() != ErrReadOnly {
		t.Fail()
	}

	cnt, err := Count(db)
	if err != nil {
		t.Fatal(err)
	}

	if cnt != 1 {
		t.Fail()
	}

	err = p.Delete()
	if err != nil {
		t.Fatal(err)
	}

	cnt, err = Count(db)
	if err != nil {
		t.Fatal(err)
	}

	if cnt != 0 {
		t.Fail()
	}

	tags, err := Tags(db, "")
	if err != nil {
		t.Fatal(err)
	}

	if len(tags) != 2 || tags[0] != "TAG-B" || tags[1] != "TAG-Z" {
		t.Fail()
	}

	tags, err = Tags(db, "tag-")
	if err != nil {
		t.Fatal(err)
	}

	if len(tags) != 2 {
		t.Fail()
	}

	tags, err = Tags(db, "taG-b")
	if err != nil {
		t.Fatal(err)
	}

	if len(tags) != 1 {
		t.Fail()
	}

	tags, err = Tags(db, "taGo")
	if err != nil {
		t.Fatal(err)
	}

	if len(tags) != 0 {
		t.Fail()
	}
}
