package password

const (
	createEntryTable = `
		create table if not exists entry (
			id integer primary key,
			name text not null unique collate nocase,
			pass blob not null
		)`

	createPrevPassTable = `
		create table if not exists prevpass (
			id integer primary key,
			eid integer not null,
			pass text not null,
			ord integer not null,
			foreign key(eid) references entry(id)
		)`

	createTagTable = `
		create table if not exists tag (
			id integer primary key,
			name text not null unique collate nocase
		)`

	createEntryTagTable = `
		create table if not exists entry_tag (
			eid integer not null,
			tid integer not null,
			primary key(eid, tid),
			foreign key(eid) references entry(id),
			foreign key(tid) references tag(id)
		)`

	createDataTable = `
		create table if not exists data (
			eid integer not null,
			key text not null collate nocase,
			encrypted integer not null,
			val blob not null,
			primary key(eid, key),
			foreign key(eid) references entry(id)
		)`

	entryByID     = `select id, name, pass from entry where id=?`
	entryByName   = `select id, name, pass from entry where name=?`
	entryTags     = `select name from tag t, entry_tag et where t.id=et.tid and et.eid=? order by name`
	entryPrevPass = `select pass from prevpass where eid=? order by ord`
	entryData     = `select key, encrypted, val from data where eid=? order by key`

	deleteEntryTags     = `delete from entry_tag where eid=?`
	deleteEntryPrevPass = `delete from prevpass where eid=?`
	deleteEntryData     = `delete from data where eid=?`
	deleteEntry         = `delete from entry where id=?`

	insertEntry    = `insert into entry(name, pass) values(?, ?)`
	insertPrevPass = `insert into prevpass(eid,pass,ord) values(?, ?, ?)`
	insertData     = `insert into data(eid,key,encrypted,val) values(?,?,?,?)`
	insertTag      = `insert or ignore into tag(name) values(?)`
	insertEntryTag = `insert or ignore into entry_tag(eid,tid) select ?, id from tag where name=?`
	updateEntry    = `update entry set name=?, pass=? where id=?`
	countEntries   = `select count(1) from entry`

	allTags    = `select name from tag order by name`
	searchTags = `select name from tag where name like ? || '%' order by name`

	searchEntries = `
		select id,
		       name
		  from entry
		 where name like ?1 || '%'
		union
		select e.id,
		       e.name
		  from entry     e,
		       tag       t,
		       entry_tag et
		 where e.id=et.eid
		   and t.id=et.tid
		   and t.name like ?1 || '%'
		union
		select id,
		       name
		  from entry e,
		       data  d
		 where d.encrypted = 0
		   and d.val like '%' || ?1 || '%'
		order by 2`
)
