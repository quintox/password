package password

import (
	"database/sql"

	"gitlab.com/quintox/crypto"
)

func queryStringList(db *sql.DB, encrypted bool, sql string, args ...interface{}) ([]string, error) {
	rows, err := db.Query(sql, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var list []string

	for rows.Next() {
		var s string

		if encrypted {
			var b []byte
			err = rows.Scan(&b)
			if err != nil {
				return nil, err
			}

			b, err = crypto.Decrypt(b, nil)
			if err != nil {
				return nil, err
			}

			s = string(b)
		} else {
			err = rows.Scan(&s)
			if err != nil {
				return nil, err
			}
		}

		list = append(list, s)
	}

	return list, nil
}
